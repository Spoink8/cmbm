import React from 'react';
import { IonReactRouter } from '@ionic/react-router';
import {
    IonApp,
    IonSplitPane
} from '@ionic/react';

// CM Components
import MenuComponent from "./components/Menu/MenuComponent";
import TabsComponent from "./components/Tabs/TabsComponent";

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import './theme/cm-types.css';
import './theme/global.css';
import { appTypes } from './config/APP_TYPES';

import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { ApolloProvider } from '@apollo/react-hooks';

const client = new ApolloClient({
    link: new HttpLink(),
    cache: new InMemoryCache()
});

// Init cache with CMBM params
try
{
    client.writeData({
        data: {
            app_state: {
                ...appTypes[0],
                __typename: 'app_state',
            },
            isLogged: false
        },
    });
}
catch (error)
{
    console.log(error);
}

const App: React.FC = () =>
{
    return (
        <ApolloProvider client={client}>
            <IonApp>
                <IonReactRouter>
                    <MenuComponent />
                    <IonSplitPane contentId="main" />
                    <TabsComponent />
                </IonReactRouter>
            </IonApp>
        </ApolloProvider>
    )
};

export default App;
