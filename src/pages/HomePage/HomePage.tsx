import React from 'react';
import {
    IonContent,
    IonPage,
}
    from '@ionic/react';
import './HomePage.css';
import '../../components/MainCard/MainCardComponent';
import DispatchCardComponent from "../../components/DispatchCard/DispatchCardComponent";
import PageHeaderComponent from "../../components/PageHeader/PageHeaderComponent";


const HomePage: React.FC = () => {

    return (
        <IonPage>
            <PageHeaderComponent />
            <IonContent>
                <DispatchCardComponent />
            </IonContent>
        </IonPage>
    );
};

export default HomePage;
