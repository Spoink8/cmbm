import React from 'react';
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonButtons,
    IonMenuButton,
}
from '@ionic/react';

// CM Styles
import './FunnyPage.css';

// CM Components
import '../../components/Menu/MenuComponent';


const FunnyPage: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Les plus drôles</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
            </IonContent>
        </IonPage>
    );
};

export default FunnyPage;
