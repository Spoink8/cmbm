import React from 'react';
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonButtons,
    IonMenuButton,
}
from '@ionic/react';

// CM Styles
import './AwkwardPage.css';

// CM Components
import '../../components/Menu/MenuComponent';


const AwkwardPage: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Les plus choquantes</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>

            </IonContent>
        </IonPage>
    );
};

export default AwkwardPage;
