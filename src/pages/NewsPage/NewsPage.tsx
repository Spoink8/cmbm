import React from 'react';
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonButtons,
    IonMenuButton,
}
from '@ionic/react';
import './NewsPage.css';
import '../../components/MainCard/MainCardComponent';
import DispatchCardComponent from "../../components/DispatchCard/DispatchCardComponent";


const NewsPage: React.FC = () => {

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Les nouvelles</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
                <DispatchCardComponent />
            </IonContent>
        </IonPage>
    );
};

export default NewsPage;
