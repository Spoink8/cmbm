import React from 'react';
import {
    IonContent,
    IonPage,
    IonItem,
    IonIcon
}
    from '@ionic/react';
import './MenuCMPage.css';

import {useApolloClient, useQuery} from "@apollo/react-hooks";
import {useLocation} from "react-router-dom";
import {GET_APP_STATE} from "../../gql/AppState.query";

import { ChangeType } from '../../utils/ChangeType';
import { ChangeConf } from '../../utils/ChangeConf';
import { appTypes } from '../../config/APP_TYPES';
import PageHeaderComponent from "../../components/PageHeader/PageHeaderComponent";


const MenuCMPage: React.FC = () => {

    const client = useApolloClient();
    let location = useLocation();

    let pathRoute :string;
    pathRoute = (location.pathname === '/add' || location.pathname === '/profile' ) ? location.pathname : '/home';

    const { loading, error, data } = useQuery(GET_APP_STATE);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return (
        <IonPage>
            <PageHeaderComponent />
            <IonContent className={'menuCM-container'}>

                {appTypes.map((appType, index) => {

                   const srcPath = (appType.enable) ? appType.logo_path : 'ComingSoon.svg';

                    return (
                        <IonItem
                            key={index}
                            lines="none"
                            detail={false}
                            routerLink={pathRoute}
                            routerDirection="none"
                            onClick={() => { ChangeType(appType.class_tag); ChangeConf(appType.class_tag, client);  }}
                            disabled={!appType.enable}
                        >
                            <IonIcon
                                className={'menuCM-icon'}
                                src={'assets/svg/logos-texts/' + srcPath }
                            />
                        </IonItem>
                    );
                })}

            </IonContent>
        </IonPage>
    );
};

export default MenuCMPage;
