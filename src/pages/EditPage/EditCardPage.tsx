import React from 'react';
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonButtons,
    IonMenuButton,
}
from '@ionic/react';

// CM Styles
import './EditCardPage.css';

// CM Components
import '../../components/Menu/MenuComponent';
import EditCardComponent from "../../components/EditCard/EditCardComponent";
import PageHeaderComponent from "../../components/PageHeader/PageHeaderComponent";


const EditPage: React.FC = () => {
    return (
        <IonPage>
            <PageHeaderComponent />

            <IonContent>
                <EditCardComponent />
            </IonContent>
        </IonPage>
    );
};

export default EditPage;
