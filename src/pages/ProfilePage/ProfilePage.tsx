import React from 'react';
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonButtons,
    IonMenuButton,
}
from '@ionic/react';
import './ProfilePage.css';

// CM Components
import '../../components/Menu/MenuComponent';
import PageHeaderComponent from "../../components/PageHeader/PageHeaderComponent";
import ProfileCardComponent from "../../components/ProfileCard/ProfileCardComponent";


const ProfilePage: React.FC = () => {
    return (
        <IonPage>
            <PageHeaderComponent />
            <IonContent>
                <ProfileCardComponent />
            </IonContent>
        </IonPage>
    );
};

export default ProfilePage;
