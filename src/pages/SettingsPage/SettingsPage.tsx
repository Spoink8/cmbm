import React from 'react';
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonButtons,
    IonMenuButton,
}
from '@ionic/react';
import './SettingsPage.css';


const SettingsPage: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Settings Page</IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent>
            </IonContent>
        </IonPage>
    );
};

export default SettingsPage;
