interface Types {
    class_tag: string;
    title: string;
    sub_title: string;
    url_redirect: string;
    logo_path: string;
    enable: boolean;
    font: string;
    url_site: string;
};

export const appTypes: Types[] = [
    {
        class_tag : 'cmbm',
        title: "C'est ma belle mère",
        sub_title: "ma belle mère",
        url_redirect: '/news',
        logo_path : 'CMaBelleMere.svg',
        enable: true,
        font: '',
        url_site: 'www.cmabellemere.fr'
    },
    {
        class_tag: 'cmpr',
        title: "C'est mon prof",
        sub_title: "mon prof",
        url_redirect: '/news',
        logo_path : 'CMonProf.svg',
        enable: true,
        font: '',
        url_site: 'www.cmonprof.fr'
    },
    {
        class_tag: 'cmgo',
        title: "C'est mon gosse",
        sub_title: "mon gosse",
        url_redirect: '/news',
        logo_path : 'CMonGosse.svg',
        enable: false,
        font: '',
        url_site: 'www.cmongosse.fr'
    },
    // {
    //     class_tag: 'cmvi',
    //     title: "C'est ma vie",
    //     sub_title: "ma vie",
    //     url_redirect: '/news',
    //     logo_path : 'CMaVie.svg',
    //     enable: false,
    //     font: '',
    //     url_site: 'www.cmavie.fr'
    // },
    {
        class_tag: 'cmme',
        title: "C'est mon mec",
        sub_title: "mon mec",
        url_redirect: '/news',
        logo_path : 'CMonMec.svg',
        enable: false,
        font: '',
        url_site: 'www.cmonmec.fr'
    },
    {
        class_tag: 'cmbp',
        title: "C'est mon beau père",
        sub_title: "mon beau père",
        url_redirect: '/news',
        logo_path : 'CMonBeauPere.svg',
        enable: false,
        font: '',
        url_site: 'www.cmonbeauf.fr'
    },
    {
        class_tag: 'cmbo',
        title: "C'est mon boss",
        sub_title: "mon boss",
        url_redirect: '/news',
        logo_path : 'CMonBoss.svg',
        enable: false,
        font: '',
        url_site: 'www.cmonboss.fr'
    },
    {
        class_tag: 'cmex',
        title: "C'est mon Ex",
        sub_title: "mon Ex",
        url_redirect: '/news',
        logo_path : 'CMonEx.svg',
        enable: false,
        font: '',
        url_site: 'www.cmonex.fr'
    },
    {
        class_tag: 'cmbl',
        title: "C'est ma blague ",
        sub_title: "ma blague",
        url_redirect: '/news',
        logo_path : 'CMaBlague.svg',
        enable: false,
        font: '',
        url_site: 'www.cmablague.fr'
    },
    {
        class_tag: 'cmbe',
        title: "C'est mon beauf",
        sub_title: "mon beauf",
        url_redirect: '/news',
        logo_path : 'CMonBeauf.svg',
        enable: false,
        font: '',
        url_site: 'www.cmonbeauf.fr'
    }
];
