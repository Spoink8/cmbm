
export function ChangeType ( pClassTag:string )
{
    var style = getComputedStyle(document.body);
    var codePrimary =  style.getPropertyValue('--ion-color-' + pClassTag + '-primary');
    var codeSecondary =  style.getPropertyValue('--ion-color-' + pClassTag + '-secondary');

    document.body.style.setProperty('--ion-color-primary', codePrimary);
    document.body.style.setProperty('--ion-color-secondary', codeSecondary);
}