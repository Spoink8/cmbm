import {appTypes} from "../config/APP_TYPES";


export async function ChangeConf (pClassTag:string, pClient:any)
{
    let newConf;

    for (let i = 0; i < appTypes.length; i++)
    {
        if (appTypes[i].class_tag === pClassTag) { newConf = appTypes[i]; break; }
    }

    /* writing cache */
    try
    {
        await
            pClient.writeData({
            data: {
                app_state: {
                    ...newConf,
                    __typename: 'app_state'
                }
            }
        });
    }
    catch (error)
    {
        throw error;
    }
}