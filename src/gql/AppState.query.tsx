import gql from "graphql-tag";

export const GET_APP_STATE = gql`
    query AppStateQuery
    {
        app_state @client 
        {
            class_tag
            title,
            sub_title,
            url_redirect,
            logo_path,
            font,
            enable,
            url_site
        }   
    }
`;