import
{
    IonCard,
    IonCardContent,
    IonIcon,
    IonLabel,
    IonButton,
    IonItem,
    IonInput,
    IonList,
    IonTextarea,
    IonCheckbox,
}
    from '@ionic/react';

import React, { useState } from 'react';
import './EditCardComponent.css';


const EditCardComponent: React.FC = () => {

    const [text, setText] = useState<string>();
    const [titre, setTitre] = useState<string>();

    return (
        <IonCard className={'edit-card'}>
            <IonCardContent>
                <IonList>
                    <IonItem>
                        <IonLabel position="stacked">Titre</IonLabel>
                        <IonInput value={titre}></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Anecdote</IonLabel>
                        <IonTextarea
                            placeholder="Votre texte ici ..."
                            value={text}
                            onIonChange={e => setText(e.detail.value!)}
                            auto-grow={'true'}
                        >
                        </IonTextarea>
                    </IonItem>
                    <IonItem>
                        <IonCheckbox slot="start" />
                        <IonLabel className="edit-card-check-label ion-text-wrap">Je veux que mon anecdote soit anonyme</IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonCheckbox slot="start" />
                        <IonLabel className="edit-card-check-label ion-text-wrap">J'ai lu et j'accepte les conditions générales d'utilisation.</IonLabel>
                    </IonItem>
                </IonList>
            </IonCardContent>

            <IonCardContent>
                <IonButton
                    slot="start"
                    onClick={() => { }}
                    className={'edit-card-btn-send'}
                    expand="full"
                >
                    <IonIcon src={'assets/svg/btn-icon/plume.svg'} />
                    <IonLabel>SOUMETTRE L'ANECDOTE</IonLabel>
                </IonButton>
            </IonCardContent>
        </IonCard>
    );
};


export default EditCardComponent;
