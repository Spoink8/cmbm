import
{
    IonCard,
    IonCardContent,
    IonIcon,
    IonLabel,
    IonButton,
    IonItem,
    IonInput,
    IonList,
    IonTextarea,
    IonCheckbox,
    IonItemDivider,
}
    from '@ionic/react';

import React, { useState } from 'react';
import './ProfileCardComponent.css';


const ProfileCardComponent: React.FC = () => {

    const [text, setText] = useState<string>();
    const [titre, setTitre] = useState<string>();

    return (
        <IonCard className={'profile-card'}>
            <IonCardContent>
                <IonList>
                    <IonItemDivider color="secondary">
                        <IonLabel>Changer de mot de passe</IonLabel>
                    </IonItemDivider>
                    <IonItem>
                        <IonLabel position="stacked">Ancien mot de passe</IonLabel>
                        <IonInput value={titre}></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Nouveau mot de passe</IonLabel>
                        <IonInput value={titre}></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Confirmation mot de passe</IonLabel>
                        <IonInput value={titre}></IonInput>
                    </IonItem>
                    <IonItemDivider color="secondary">
                        <IonLabel>Changer l'adresse mail</IonLabel>
                    </IonItemDivider>
                    <IonItem>
                        <IonLabel position="stacked">Mail</IonLabel>
                        <IonInput value={titre}></IonInput>
                    </IonItem>
                    <IonItemDivider color="secondary">
                        <IonLabel>S'abonner aux newsletters</IonLabel>
                    </IonItemDivider>
                    <IonItem lines={'none'}>
                        <IonCheckbox slot="start" />
                        <IonLabel className="profile-card-check-label ion-text-wrap">J'accepte de recevoir les communications du site Cmabellemere.fr</IonLabel>
                    </IonItem>
                    <IonItem lines={'none'}>
                        <IonCheckbox slot="start" />
                        <IonLabel className="profile-card-check-label ion-text-wrap">J'accepte de recevoir les communications des partenaires du site Cmabellemere.fr</IonLabel>
                    </IonItem>
                    <IonItem lines={'none'}>
                        <IonButton
                            className="profile-card-delete-btn"
                            onClick={() => { }}
                            fill={'outline'}
                        >
                            <IonLabel>Supprimer le compte</IonLabel>
                        </IonButton>
                    </IonItem>
                </IonList>
            </IonCardContent>

            <IonCardContent>
                <IonButton
                    slot="start"
                    onClick={() => { }}
                    className={'profile-card-btn-send'}
                    expand="full"
                >
                    <IonIcon src={'assets/svg/btn-icon/save.svg'} />
                    <IonLabel>Sauvegarder</IonLabel>
                </IonButton>
            </IonCardContent>
        </IonCard>
    );
};


export default ProfileCardComponent;
