import React from 'react';
import {
    IonHeader,
    IonTitle,
    IonToolbar,
    IonButtons,
    IonMenuButton, IonIcon,
}
    from '@ionic/react';

import './PageHeaderComponent.css';
import {useApolloClient, useQuery} from "@apollo/react-hooks";
import {useLocation} from "react-router-dom";
import {GET_APP_STATE} from "../../gql/AppState.query";


const PageHeaderComponent: React.FC = () => {

    const client = useApolloClient();
    let location = useLocation();

    let pathRoute :string;
    pathRoute = (location.pathname === '/add' || location.pathname === '/profile' ) ? location.pathname : '/home';

    const { loading, error, data } = useQuery(GET_APP_STATE);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return (
        <IonHeader className={'main-header'}>
            <IonToolbar color="primary">
                <IonButtons slot="start">
                    <IonMenuButton />
                </IonButtons>
                <IonTitle text-center>
                    <IonIcon src={'assets/svg/logos-horizontals/on/' + data.app_state.logo_path } />
                    {/*<span>{data.app_state.sub_title}</span>*/}
                </IonTitle>
            </IonToolbar>
        </IonHeader>
    );
};

export default PageHeaderComponent;
