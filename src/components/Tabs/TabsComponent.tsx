import
{
    IonIcon,
    IonLabel, IonRouterOutlet,
    IonTabBar,
    IonTabButton,
    IonTabs
} from '@ionic/react';

import React from 'react';
import {Redirect, Route, useLocation} from "react-router-dom";

import './TabsComponent.css';

import NewsPage from "../../pages/NewsPage/NewsPage";
import FunnyPage from "../../pages/FunnyPage/FunnyPage";
import AwkwardPage from "../../pages/AwkwardPage/AwardsPage";
import ProfilePage from "../../pages/ProfilePage/ProfilePage";
import EditPage from "../../pages/EditPage/EditCardPage";
import HomePage from "../../pages/HomePage/HomePage";
import MenuCMPage from "../../pages/MenuCMPage/MenuCMPage";

import {useQuery} from "@apollo/react-hooks";
import {GET_APP_STATE} from "../../gql/AppState.query";

const TabsComponent: React.FC = () => {

   // Check route location, for disabling tabs if needed
    let noTabs = false;
    let location = useLocation();
    if (location.pathname === '/menu') noTabs = true;

    const { loading, error, data } = useQuery(GET_APP_STATE);
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return (
        <IonTabs>
            <IonRouterOutlet id="main">
                <Route path="/home" component={HomePage} exact={true}/>
                <Route path="/add" component={EditPage} exact={true}/>
                <Route path="/profile" component={ProfilePage} exact={true}/>
                <Route path="/menu" component={MenuCMPage} exact={true}/>
                <Route path="/" render={() => <Redirect to="/home" />} exact={true}/>
            </IonRouterOutlet>
            <IonTabBar slot="bottom" hidden={noTabs}>
                <IonTabButton tab="home" href="/home">
                    <IonIcon class="icon cm-tab-icon" src="assets/svg/logos-tabs/tab-logo-nofill.svg" />
                    {/*<IonLabel class="cm-tab-label">C nouveau</IonLabel>*/}
                </IonTabButton>
                <IonTabButton tab="add" href="/add">
                    <IonIcon class="icon cm-tab-icon" src="assets/svg/btn-icon/more-outline.svg" />
                    {/*<IonLabel class="cm-tab-label">C drôle</IonLabel>*/}
                </IonTabButton>
                <IonTabButton tab="profile" href="/profile">
                    <IonIcon class="icon cm-tab-icon" src="assets/svg/btn-icon/profile-outline.svg" />
                    {/*<IonLabel class="cm-tab-label">C choquant</IonLabel>*/}
                </IonTabButton>
            </IonTabBar>
        </IonTabs>
    );
};

export default TabsComponent;