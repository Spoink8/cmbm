import
{
    IonSegment,
    IonSegmentButton,
    IonLabel,
    IonIcon,
}
from '@ionic/react';

import React from 'react';
import './DispatchCardComponent.css';

import '../../components/MainCard/MainCardComponent';
import MainCardComponent from "../MainCard/MainCardComponent";

interface NetworkMockProps {
    network_mock?: any
}

const DispatchCardComponent: React.FC<NetworkMockProps> = ({network_mock}) => {

    return (
        <div id='dispatch-card-component'>
            <div className='filterContainer'>
                <IonSegment onIonChange={e => console.log('Segment selected', e.detail.value)}>
                    <IonSegmentButton value="new">
                        <IonLabel>Nouvelles</IonLabel>
                        <IonIcon src={'assets/svg/btn-icon/fire-outline.svg'} />
                    </IonSegmentButton>
                    <IonSegmentButton value="funny">
                        <IonLabel>Drôles</IonLabel>
                        <IonIcon src={'assets/svg/btn-icon/laugh-beam-regular.svg'} />
                    </IonSegmentButton>
                    <IonSegmentButton value="awkward">
                        <IonLabel>Choquantes</IonLabel>
                        <IonIcon src={'assets/svg/btn-icon/surprise-regular.svg'} />
                    </IonSegmentButton>
                </IonSegment>

            </div>
            <div className="columnFix">
                { network_mock.map((mock:NetworkMockProps, index:number) => {
                    return (
                        <MainCardComponent key={index} trophy={index+1} />
                    );
                })}
            </div>
        </div>
    );
};

DispatchCardComponent.defaultProps = {
    network_mock: [
        { card : 1 },
        { card : 2 },
        { card : 3 },
        { card : 4 },
        { card : 5 },
        { card : 6 },
        { card : 7 },
        { card : 1 },
        { card : 2 },
        { card : 3 },
        { card : 4 },
        { card : 5 },
        { card : 6 },
        { card : 7 },
        { card : 1 },
        { card : 2 },
        { card : 3 },
        { card : 4 },
        { card : 5 },
        { card : 6 },
        { card : 7 },
    ]
}
//
export default DispatchCardComponent;
// export default withIonLifeCycle(DispatchCardComponent);
