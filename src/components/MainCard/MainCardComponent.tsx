import
{
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonAvatar,
    IonIcon,
    IonLabel,
    IonButton,
    IonGrid,
    IonRow,
    IonCol,
    IonPopover,
    IonImg,
}
from '@ionic/react';

import React, { useState } from 'react';
import './MainCardComponent.css';

interface MainCardProps {
    trophy?: number
    data?: any
}

const MainCardComponent: React.FC<MainCardProps> = ({trophy, data}) => {

    const [showPopover, setShowPopover] = useState<{open: boolean, event: Event | undefined}>({
        open: false,
        event: undefined,
    });

    let defaultAvatar = null;
    switch(trophy)
    {
        case 1 : defaultAvatar = 'assets/svg/btn-icon/gold-medal.svg'; break;
        case 2 : defaultAvatar = 'assets/svg/btn-icon/silver-medal.svg'; break;
        case 3 : defaultAvatar = 'assets/svg/btn-icon/bronze-medal.svg'; break;
        default : break;
    }

    const isPicture = ( Math.floor(Math.random() * 10) % 2 ) ? true : false;
    const imgSrc = ( Math.floor(Math.random() * 10) % 2 )
    ? 'https://images.unsplash.com/photo-1451440063999-77a8b2960d2b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1489&q=80'
    : 'https://images.unsplash.com/photo-1526731530795-375e081e6154?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80';

    return (
        <IonCard className={'main-card'}>
            <IonCardHeader>
                <IonGrid className={'main-card-header'}>
                    <IonRow>
                        <IonCol size='8'>
                            { defaultAvatar !== null &&
                            <IonAvatar className='main-card-header-trophy'>
                                <img src={defaultAvatar} />
                            </IonAvatar>
                            }
                            <div className='main-card-header-subtitle-container'>
                                <div className='main-card-header-subtitle'>Laure020</div>
                                <div className='main-card-header-subtitle'> Aujourd'hui</div>
                            </div>
                        </IonCol>
                        <IonCol size='4' className='ion-text-end'>
                            <IonAvatar onClick={(e) => setShowPopover({open: true, event: e.nativeEvent})}>
                                <img src={'assets/svg/btn-icon/social-media.svg'} />
                            </IonAvatar>
                            <IonPopover
                                isOpen={showPopover.open}
                                event={showPopover.event}
                                onDidDismiss={ e => setShowPopover({open: false, event: undefined}) }
                                animated={true}
                            >
                                <IonButton fill="clear">
                                    <IonIcon src={'assets/svg/btn-icon/facebook.svg'} />
                                </IonButton>
                                <IonButton fill="clear">
                                    <IonIcon src={'assets/svg/btn-icon/twitter.svg'} />
                                </IonButton>
                                {/*<IonButton fill="outline" color={'dark'}>*/}
                                {/*    <IonIcon src={'assets/svg/btn-icon/social-media.svg'} />*/}
                                {/*    <IonLabel>Partager</IonLabel>*/}
                                {/*</IonButton>*/}
                                {/*<IonButton fill="outline" color={'danger'}>*/}
                                {/*    <IonIcon src={'assets/svg/btn-icon/signs.svg'} />*/}
                                {/*    <IonLabel>Signaler</IonLabel>*/}
                                {/*</IonButton>*/}
                            </IonPopover>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol size='12'>
                            <IonCardTitle>Lorem ipsum</IonCardTitle>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonCardHeader>

            <IonCardContent>
                <IonGrid className={'main-card-body'}>
                    {/* No Picture = full text all screen */}
                    { !isPicture &&
                        <IonRow>
                            <IonCol size='12'>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            </IonCol>
                        </IonRow>
                    }
                    {/* With Picture = full text and under full image on xs, after half text along half picture */}
                    { isPicture &&
                        <IonRow>
                            <IonCol size-xs='12'  size-sm='6' size-md='12' size-lg='12' size-xl='12'>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            </IonCol>
                            <IonCol size-xs='12' size-sm='6' size-md='12' size-lg='12' size-xl='12'>
                                <IonImg src={imgSrc} />
                            </IonCol>
                        </IonRow>
                    }
                </IonGrid>
            </IonCardContent>

            <IonCardContent>
                <IonGrid className={"main-card-ctrl"}>
                    <IonRow>
                        <IonCol size='8'>
                            <IonButton  fill="solid">
                                <IonIcon src={'assets/svg/btn-icon/laugh-beam-regular.svg'}  />
                                <IonLabel class="hidden-xxs-down">23</IonLabel>
                            </IonButton>
                            <IonButton fill="outline">
                                <IonIcon src={'assets/svg/btn-icon/surprise-regular.svg'} />
                                <IonLabel class="hidden-xxs-down">14</IonLabel>
                            </IonButton>
                        </IonCol>
                        <IonCol size='4'>
                            <IonButton fill="outline">
                                <IonIcon src={'assets/svg/btn-icon/comment-pen.svg'} />
                                <IonLabel class="hidden-xxs-down">14</IonLabel>
                                <IonLabel class="hidden-xxs-down hidden-xs-between hidden-md-between hidden-xl-between">commentaires</IonLabel>
                            </IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonCardContent>
        </IonCard>
    );
};

MainCardComponent.defaultProps = {
    trophy: 0,
    data: {}
}

export default MainCardComponent;
