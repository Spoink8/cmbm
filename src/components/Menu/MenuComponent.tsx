import {
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonMenu,
    IonMenuToggle,
    IonButton
} from '@ionic/react';

import React from 'react';
import './MenuComponent.css';
import { ChangeType } from '../../utils/ChangeType';
import { ChangeConf } from '../../utils/ChangeConf';
import { appTypes } from '../../config/APP_TYPES';

import {useApolloClient, useQuery} from "@apollo/react-hooks";
import {GET_APP_STATE} from "../../gql/AppState.query";
import {useLocation} from "react-router-dom";

import { globeOutline } from 'ionicons/icons';

const MenuComponent: React.FC = () => {

    const client = useApolloClient();
    let location = useLocation();

    let pathRoute :string;
    pathRoute = (location.pathname === '/add' || location.pathname === '/profile' ) ? location.pathname : '/home';

    const { loading, error, data } = useQuery(GET_APP_STATE);
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    console.log(data);

    return (
        <IonMenu contentId="main" type="overlay">
            <IonContent>
                <IonList id="menu-list"  lines="none">
                    <IonMenuToggle autoHide={false}>
                        <IonItem className='arrow-detail-color' routerLink="/menu" detail>
                            <IonIcon src={"assets/svg/logos-horizontals/on/CMaVie.svg"} />
                            {/*<IonLabel className={'menu-header-label'} >ma vie</IonLabel>*/}
                        </IonItem>
                    </IonMenuToggle>

                    <IonItem text-center vertical-center >
                        <IonIcon src={globeOutline} />
                        <IonLabel>{data.app_state.url_site}</IonLabel>
                    </IonItem>

                    {/* Link Page Part */}
                    <IonMenuToggle autoHide={false}>
                        <IonItem
                            className={'menu-body-item'}
                            lines="none"
                            detail={false}
                            color={'transparent'}
                            routerLink={'/profile'} routerDirection="none"
                        >
                            <IonButton
                                slot="start"
                                onClick={() => { }}
                                className={'menu-body-btn page_button'}
                                color='transparent'
                                fill="clear"
                            >
                                <IonIcon src={'assets/svg/btn-icon/profile-outline.svg'} className={'page_button_icon'} />
                                <IonLabel className={''}>Profile</IonLabel>
                            </IonButton>
                        </IonItem>
                    </IonMenuToggle>
                    <IonMenuToggle autoHide={false}>
                        <IonItem
                            className={'menu-body-item'}
                            lines="none"
                            detail={false}
                            color={'transparent'}
                            routerLink={'/'} routerDirection="none"
                        >
                            <IonButton
                                slot="start"
                                onClick={() => { }}
                                className={'menu-body-btn page_button'}
                                color='transparent'
                                fill="clear"
                            >
                                <IonIcon src={'assets/svg/btn-icon/plume.svg'} className={'page_button_icon'} />
                                <IonLabel className={''}>Mes anecdotes</IonLabel>
                            </IonButton>
                        </IonItem>
                    </IonMenuToggle>
                    <IonMenuToggle autoHide={false}>
                        <IonItem
                            className={'menu-body-item'}
                            lines="none"
                            detail={false}
                            color={'transparent'}
                            routerLink={'/'} routerDirection="none"
                        >
                            <IonButton
                                slot="start"
                                onClick={() => { }}
                                className={'menu-body-btn page_button'}
                                color='transparent'
                                fill="clear"
                            >
                                <IonIcon src={'assets/svg/btn-icon/logout.svg'} className={'page_button_icon'} />
                                <IonLabel className={''}>Logout</IonLabel>
                            </IonButton>
                        </IonItem>
                    </IonMenuToggle>

                    {/*Mini-link to subcategories*/}
                    {/*{appTypes.map((appType, index) => {*/}

                    {/*    return (*/}
                    {/*        <IonMenuToggle key={index} autoHide={false}>*/}
                    {/*            <IonItem*/}
                    {/*                className={'menu-body-item'}*/}
                    {/*                lines="none"*/}
                    {/*                detail={false}*/}
                    {/*                color={ (data.app_state.class_tag === appType.class_tag ) ? appType.class_tag+'-secondary' : 'transparent'}*/}
                    {/*                routerLink={pathRoute} routerDirection="none"*/}
                    {/*            >*/}
                    {/*                <IonButton*/}
                    {/*                    slot="start"*/}
                    {/*                    onClick={() => { ChangeType(appType.class_tag); ChangeConf(appType.class_tag, client);  }}*/}
                    {/*                    color={appType.class_tag + '-primary' }*/}
                    {/*                    className={appType.class_tag + '_button menu-body-btn' }*/}
                    {/*                    >*/}
                    {/*                    <IonIcon src={ 'assets/svg/logos/' + appType.logo_path} className={appType.class_tag + '_icon' } />*/}
                    {/*                    <IonLabel className={appType.class_tag + '_label' } >{appType.sub_title}</IonLabel>*/}
                    {/*                </IonButton>*/}
                    {/*            </IonItem>*/}
                    {/*        </IonMenuToggle>*/}
                    {/*    );*/}
                    {/*})}*/}

                    {/*Mono-link subcategories*/}
                    {/*<IonMenuToggle autoHide={false}>*/}
                    {/*    <IonItem*/}
                    {/*        className={'menu-body-item'}*/}
                    {/*        lines="none"*/}
                    {/*        detail={false}*/}
                    {/*        color={ data.app_state.class_tag+'-secondary'}*/}
                    {/*        // routerLink={pathRoute} routerDirection="none"*/}
                    {/*        routerLink="/menu"*/}
                    {/*    >*/}
                    {/*        <IonButton*/}
                    {/*            slot="start"*/}
                    {/*            // onClick={() => { ChangeType(data.app_state.class_tag); ChangeConf(data.app_state.class_tag, client);  }}*/}
                    {/*            color={data.app_state.class_tag + '-primary' }*/}
                    {/*            className={data.app_state.class_tag + '_button menu-body-btn' }*/}
                    {/*        >*/}
                    {/*            <IonIcon src={'assets/svg/logos/' + data.app_state.logo_path} className={data.app_state.class_tag + '_icon' } />*/}
                    {/*            <IonLabel className={data.app_state.class_tag + '_label' } >{data.app_state.sub_title}</IonLabel>*/}
                    {/*        </IonButton>*/}
                    {/*    </IonItem>*/}
                    {/*</IonMenuToggle>*/}


                </IonList>
            </IonContent>
        </IonMenu>
    );
};

export default MenuComponent;
